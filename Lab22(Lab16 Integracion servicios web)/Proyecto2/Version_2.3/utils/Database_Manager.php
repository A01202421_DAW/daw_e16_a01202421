<?php
header("Content-Type: text/html;charset=utf-8");

class Database_Manager
{
    private $estados = array(
    -5 => 'No se pudo actualizar la base de datos',
    -4 => 'No se pudo eliminar el elemento',
    -3 => 'El query no regreso ningun valor',
	-2 => 'No se pudo conectar con la base de datos', 
	-1 => 'No se pudo conectar con el servidor', 
	 0 => 'Default', 
	 1 => 'Conectado con el servidor',
     2 => 'Conectado con la base de datos',
     3 => 'Se elimino con exito',
     4 => 'Se actualizo la base de datos'
	);
    
    private $db_link;
    private $db_actual;
    private $estado;
    private $debugging;
    
    public function __construct($host, $usuario, $pass)
	{
        if(isset($_SESSION['debugging']))
        {
            $this->debugging = $_SESSION['debugging'];
        }else
        {
            $this->debugging = false;
        }
		$this->estado = 0;
		$this->conectarConServidor($host, $usuario, $pass);
	}
    
    public function conectarConServidor($host, $usuario, $pass)
    {
        $this->db_link = mysql_connect($host, $usuario, $pass);
		if (!$this->db_link)
		{
            echo "No pudo conectarse: " . mysql_error();
			$this->estado = -1;
			return;
		}
		$this->estado = 1;
    }
        
    public function conectarConDB($baseDeDatos)
    {
        if($this->estado > 0)
		{
			$this->db_actual = mysql_select_db($baseDeDatos, $this->db_link);
            mysql_query("SET NAMES 'utf8'");
            if(!$this->db_actual)
            {
                echo "No pudo conectarse: " . mysql_error();
                $this->estado = -2;
                return;
            }
            $this->estado = 2;	
		}
    }
    
    private function print_output($arg)
    {
        if($this->debugging)
        {
            print_r($arg);
            echo "<br/>";
        }
    }
        
    public function getEstado()
	{
		return $this->estado;
	}
    
    
    public function obtener_valor($nombre_tabla, $key, $value, $output)
    {
        $aux = $this->obtenerArregloDeQuery($nombre_tabla, array($key => $value));
        return $aux[0][$output];
    }
    
    public function selectQuery($nombre_tabla, /*array*/ $valores, /*OPTIONAL array*/ $like = null, /*OPTIONAL*/ $sort = null, /*OPTIONAL array*/ $gt_lt_eq = null, /*Optional*/ $limit = null, /*Optional*/ $number_items = null)
    {
        if($valores == 0)
		{
			$queryString = "SELECT * from " . $nombre_tabla;
		}else
		{
            if($gt_lt_eq != null)
            {
                $keys_gt_lt_eq = array_keys($gt_lt_eq);
            }
            $valoresString = array_map('mysql_real_escape_string', array_values($valores));
            $keys = array_keys($valores);
            $queryString = "SELECT * from " . $nombre_tabla . " WHERE ";
			$cont = 0;
            $i = 0;
            
            for($i = 0; $i < count($valoresString)-1; $i++)
            {
                if(in_array($keys[$i], $like))
                {
                    $queryString = $queryString ."LOWER(". $keys[$i]. ") like LOWER('%" . $valoresString[$i] . "%') AND ";
                }else if (in_array($keys[$i], $keys_gt_lt_eq))
                {
                    if($gt_lt_eq[$keys[$i]] < 0)
                    {
                        $queryString = $queryString . $keys[$i]. "<='" . $valoresString[$i] . "' AND ";
                    }else{
                        $queryString = $queryString . $keys[$i]. ">='" . $valoresString[$i] . "' AND ";
                    }
                }else{
                    $queryString = $queryString . $keys[$i]. "='" . $valoresString[$i] . "' AND ";
                }
            }
            if($gt_lt_eq != null)
            {
                if (in_array($keys[$i], $keys_gt_lt_eq))
                {
                    if($gt_lt_eq[$keys[$i]] < 0)
                    {
                        $queryString = $queryString . $keys[$i]. "<='" . $valoresString[$i] . "'";
                    }else{
                        $queryString = $queryString . $keys[$i]. ">='" . $valoresString[$i] . "'";
                    }
                }else if($like != null)
                {
                    if(in_array($keys[count($valoresString)-1], $like))
                    {
                        $queryString = $queryString ."LOWER(". $keys[$i]. ") like LOWER('%" . $valoresString[$i] . "%')";
                    }else
                    {
                        $queryString = $queryString . $keys[$i]. "='" . $valoresString[$i] . "'";
                    }
                }
            }else if($like != null)
            {
                if(in_array($keys[count($valoresString)-1], $like))
                {
                    $queryString = $queryString ."LOWER(". $keys[$i]. ") like LOWER('%" . $valoresString[$i] . "%')";
                }else
                {
                    $queryString = $queryString . $keys[$i]. "='" . $valoresString[$i] . "'";
                }
            }
            else{
                $queryString = $queryString . $keys[$i]. "='" . $valoresString[$i] . "'";
            }
        }
        if(isset($sort))
        {
            $queryString = $queryString . " ORDER BY ".$sort." DESC ";
        }
        if(isset($limit))
        {
            $queryString = $queryString . " LIMIT ".$limit.", ".$number_items;
        }
        $this->print_output($queryString);
        $resultado = mysql_query($queryString, $this->db_link);
        return $resultado;
    }
    
    public function obtenerArregloDeQuery($nombre_tabla, /*array*/ $valores, /*OPTIONAL array*/ $like = null, /*OPTIONAL*/ $sort = null, /*OPTIONAL array*/ $gt_lt_eq = null, /*Optional*/ $limit = null, /*Optional*/ $number_items = null)
    {
        if($this->estado != 2)
		{
            return $this->estado;
        }
        $this->print_output($like);
        $resultado = array();    
        $resultado_Query = $this->selectQuery($nombre_tabla, $valores, $like, $sort,$gt_lt_eq, $limit, $number_items);
        if (mysql_num_rows($resultado_Query) > 0) 
        {
            while ($row = mysql_fetch_array($resultado_Query)) 
            {
				array_push($resultado, $row);
            }
        }else
        {
            return -3;
        }
        $this->print_output($resultado);
		return $resultado;
    }
    
    public function verificarSiExiste($nombre_tabla, /*array*/ $valores)
    {
        if($this->estado != 2)
		{
            return 0;
        }
        $resultado_Query = $this->selectQuery($nombre_tabla, $valores);
        if( mysql_num_rows( $resultado_Query ) == 0)
		{			
            return -1;
		}
		return 1;
    }
        
    public function buscarEnTabla($nombre_tabla, /*array*/ $valores, /*OPTIONAL array*/ $like, /*OPTIONAL*/ $sort, /*OPTIONAL array*/ $gt_lt_eq = null)
    {
        if($this->estado != 2)
		{
            return $this->estado;
        }
        $resultado = array();    
        $resultado_Query = $this->selectQuery($nombre_tabla, $valores, $like, $sort, $gt_lt_eq);
        if (mysql_num_rows($resultado_Query) > 0) 
        {
            while ($row = mysql_fetch_array($resultado_Query)) 
            {
				array_push($resultado, $row);
            }
        }else
        {
            return -3;
        }
		return $resultado;
    }
    
    
    public function insertarEnTabla($nombre_tabla,/*array*/$valores)
    {
        if($this->estado != 2)
		{
            return $this->estado;
        }
        $valoresString = array_map('mysql_real_escape_string', array_values($valores));
		$keys = array_keys($valores);
		$queryString = 'INSERT INTO `' . $nombre_tabla. '` (`' . implode('`,`' , $keys) . '`) VALUES (\'' . implode( '\',\'' , $valoresString) . '\')';
        $this->print_output($queryString);
		$result=mysql_query($queryString, $this->db_link);
    }
    
    
    public function eliminarEnTabla($nombre_tabla,/*array*/ $valores)
    {
        
        if($this->estado != 2)
		{
            return $this->estado;
        }
        
		$valoresString = array_map('mysql_real_escape_string', array_values($valores));
        $keys = array_keys($valores);

        $queryString = "DELETE from " . $nombre_tabla . " WHERE ";
		$cont = 0;
        for($i = 0; $i < count($valoresString)-1; $i++)
        {
            $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "' AND ";
        }
        $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "'"; 
        $this->print_output($queryString);
        $resultado = mysql_query($queryString, $this->db_link);
		if (mysql_affected_rows($this->db_link) > 0) 
        {
			return -4;
		}
		else 
        {
			return 3;
		}
    }
    
    public function actualizarEnTabla($nombre_tabla,/*array*/ $valores, /*array*/ $entrada)
    {    
        if($this->estado != 2)
		{
            return $this->estado;
        }
        
        $valoresString = array_map('mysql_real_escape_string', array_values($valores));
        $keys = array_keys($valores);
        $queryString = "UPDATE " . $nombre_tabla . " SET ";
        for($i = 0; $i < count($valoresString)-1; $i++)
        {
              $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "', "; 
        }
        $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "'"; 
        
        $queryString = $queryString . "WHERE ";
        
        $valoresString = array_map('mysql_real_escape_string', array_values($entrada));
        $keys = array_keys($entrada);
        
        for($i = 0; $i < count($valoresString)-1; $i++)
        {
            $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "' AND ";
        }
        $queryString = $queryString . $keys[$i] . "='" . $valoresString[$i] . "'"; 
        $this->print_output($queryString);
        $resultado = mysql_query($queryString, $this->db_link);
        

    }
    
    public function error($errorNum)
    {
        if (array_key_exists($errorNum, $estados)) 
        {
            return "ERROR " . $errorNum . ":" .$errores[$errorNum];
        }
        return "ERROR INVALIDO";
    }
    
    
    public function ejecutarQuery($query)
    {
        if($this->estado != 2)
		{
            return $this->estado;
        }
        $resultado = array();
        $this->print_output($query);
        $resultado_Query =  mysql_query($query, $this->db_link);
        if (mysql_num_rows($resultado_Query) > 0) 
        {
            while ($row = mysql_fetch_array($resultado_Query, MYSQL_BOTH ))
            {
                $this->print_output($row);
				array_push($resultado, $row);
            }
        }else
        {
            return -3;
        }
		return $resultado;
    }
    
    
    function __destruct()
	{
		//mysql_close($this->db_link);
	}
}
?>