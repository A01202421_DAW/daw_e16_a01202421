<?php

class Info
{
    
    public static $host_nombre = "localhost";
    public static $db_usuario = "a01202421_daw";
    public static $db_pass = "";
    public static $db_nombre = "inmuebles";
    
    // For LogIn;
    public static $nombre_tabla_usuarios = "users";
    public static $field_usuario = "email";
    public static $field_correo = "email";
    public static $field_contraseña = "password";
    public static $field_ultimo_conectado = "last_connected";
    public static $field_salt = "salt";
    public static $field_estado = "wrong_pass";
    public static $field_id = "id";
    public static $field_rol = "rol";
    
    // For Password reset
    
    public static $nombre_tabla_recoverPassword = "reset_password";
    public static $field_recover_id = "id_us";
    public static $field_recover_mail = "id_us";
    public static $field_recover_token = "token";
    public static $field_recover_fecha = "fecha";

    
    public static $emailLogin = true;
    public static $blockBruteForce = true;
    public static $bf_time = 300;
    
    // NO MODIFICAR!!!
    public static $password_Salt = "193kdjskl183dsjk1";
    public static $cookieKey = "asdfasddakjad828e89832";
    public static $company = "";
    public static $time = 100;
    public static $max_Attempts = 4;
    
    // Paginas que no requieren logIn
    public static $staticPages = array(
        "/Proyecto2/Version_2.3/search.php", "/Proyecto2/Version_2.3/index.php", "/Proyecto2/Version_2.3/generic.php"
    );
    
    // Pagina para loggear
    public static $loginPage = "/Proyecto2/Version_2.3/index.php";
        
    public static $phpSessionStart = true;
        
    //Pagina Home al loggear
    public static $homePage = "./usuarios/form.php";
    public static $rememberMe = true;
}

?>