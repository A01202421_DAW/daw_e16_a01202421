<?php
    require_once("./utils/Config.php");
    $ls->init();
    if(isset($_POST['act_login']))
    {
        $user=$_POST['Email'];
        $pass=$_POST['passwordinput'];
        if($user=="" || $pass=="")
        {
            $msg = array("Error", "No se ingreso usuario y/o contraseña");
        }else
        {
            $result = $ls->login($user, $pass);
            if(!$result )
            {
                $msg=array("Error", "Usuario y/o contraseña incorrectos");
            }
        }
    } 

?>

<ul>
	<li>
		<div class="container">
		<!-- User Menu -->
			<p>
		        <?php
		            if(isset($result["status"]))
		            {
		                echo "Cuenta bloqueada, espere ".$result["seconds"]." segundos";
		            }else
		            {
		                if(isset($msg))
		                {
		                    echo $msg[0]." ".$msg[1];
		                }
		            }
		        ?>
		<p>
			
		<!-- Check if a session has started-->
		<?php
			if(isset($_SESSION["Email"])):
		?>
			<a href="#menu"><button class="btn btn-primary green medium" style="float:right; margin-right: 30px;">Mi Perfil</button></a>
		<?php
			endif;
		?>
		<!-- Publicar Propiedad -->
  			<button type="button" class="btn btn-success small green" href="#publicProperty" data-toggle="modal" data-target=".bs-modal-sm" style="float:right; margin-right: 30px;">Publica tu propiedad</button>
  			
  		<!-- Login/Register -->
  			<?php
				/*if (!isset($_SESSION["Email"])):*/
			?>
  			<button class="button special small" href="#signup" data-toggle="modal" data-target=".bs-modal-sm" style="float:right; margin-right: 30px;">Entrar</button>
  			<?php
			    /*endif;*/
			?>
 		</div>
 		
 		<?php
 			if (!isset($_SESSION["Email"])):
 				include_once('_login.html'); 
 			endif;
 		?>
 	</li>
</ul>