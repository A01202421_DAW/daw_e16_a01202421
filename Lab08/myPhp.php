    <?php include_once("_header.html"); ?>
    <?php
        $bandera = 1;
        if((empty($_POST['fact']))||(empty($_POST['ipar']))|| (empty($_POST['findx']))){
            echo "Verifica que no eres un robot al contestar las preguntas matematicas.".'<br>';
            $bandera = 0;
        } else {
            if(strcmp($_POST['ipar'],"impar") != 0){
                echo "El 7 no es par. Acaso eres un robot?".'<br>';
                $bandera = 0;
                
            }
            if($_POST['fact'] != 120){
                echo "Escogiste mal el factorial de 5. Acaso eres un robot?".'<br>';
                $bandera = 0;
            }
            if($_POST['findx'] != 3){
                echo "Despejaste mal x. Acaso eres un robot?".'<br>';
                $bandera = 0;
            }
        }
        if(empty($_POST['name'])){
            echo "Nombre es obligatorio.".'<br>';
            $bandera = 0;
        }
        if(empty($_POST['email'])){
            echo "Email es obligatorio.".'<br>';
            $bandera = 0;
        }
        if(!empty($_POST['email'])&&!empty($_POST['name']))
            if ( (strcmp($_POST['name'],"Katz") != 0) || (strcmp($_POST['email'],"A01202421@itesm.mx") != 0)){
                echo "Nombre o email no valido.".'<br>';
                $bandera = 0;
            }
        if(empty($_POST['pass'])){
            echo "Password no puede estar vacio.".'<br>';
            $bandera = 0;
        }  else if (strcmp($_POST['pass'],"awesome") != 0){
            echo "Password incorrecto.".'<br>';
            $bandera = 0;
        }
        if(empty($_POST['s1'])){
            echo "Selecciona una Edad.".'<br>';
            $bandera = 0;
        } else if($_POST['s1'] < 18){
            echo "No se admiten los menores de edad.".'<br>';
            $bandera = 0;
        }
        if ($bandera)
            header("Location: _correct.html");
        
    ?>
    <?php include_once("_footer.html"); ?>