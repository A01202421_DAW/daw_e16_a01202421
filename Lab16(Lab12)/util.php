<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "fruits";
        
        $con = mysqli_connect($servername,$username,$password,$dbname);
        
        if(!$con) die("Connection failed: ".mysqli_connect_error());
        return $con;
    }
    
    function closeDb($mysql){
        mysqli_close($mysql);
    }
    function getFruits(){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
    function getFruitsByName($fruit_name){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit WHERE name LIKE '%".$fruit_name."%'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
    function getCheapestFruits($cheap_price){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit WHERE price <= '".$cheap_price."'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
    function insertFruit($name, $units, $quantity, $price, $country){
        $conn = connectDb();
        $sql = "INSERT INTO fruit (name, units, quantity, price, country) VALUES (\"".$name."\",\"".$units."\",\"".$quantity."\",\"".$price."\",\"".$country."\");";
        if(mysqli_query($conn, $sql)){
            echo "New record created successfully";
            closeDb($conn);
            return true;
        } else {
            echo "Error: ".$sql."<br>".mysqli_error($conn);
            closeDb($conn);
            return false;
        }
        closeDb($conn);
    }
    function delete_by_name($fruit_name){
        $conn = connectDb();
        $sql = "DELETE FROM fruit WHERE name = '".$fruit_name."'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
    function update_by_name($id, $name, $units, $quantity, $price, $country){
        $conn = connectDb();
        $sql = "UPDATE fruit SET name='$name',units = '$units', quantity ='$quantity', price = '$price', country = '$country' WHERE name = '$name'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
?>