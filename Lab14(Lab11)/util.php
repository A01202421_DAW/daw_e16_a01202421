<?php
    function connectDb(){
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "fruits";
        
        $con = mysqli_connect($servername,$username,$password,$dbname);
        
        if(!$con) die("Connection failed: ".mysqli_connect_error());
        return $con;
    }
    
    function closeDb($mysql){
        mysqli_close($mysql);
    }
    function getFruits(){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return $result;
    }
    function getFruitsByName($fruit_name){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit WHERE name LIKE '%".$fruit_name."%'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
    function getCheapestFruits($cheap_price){
        $conn = connectDb();
        $sql = "SELECT name, units, quantity, price, country FROM fruit WHERE price <= '".$cheap_price."'";
        $result = mysqli_query($conn,$sql);
        closeDb($conn);
        return result;
    }
?>