<!--https://daw-a01202421-daw.c9users.io/phpmyadmin-->
<?php include_once("_header.html"); ?>
<?php
    require_once "util.php";
    $result = getFruits();
    if(mysqli_num_rows($result)>0){
        echo "<table>";
        echo "<tr>";
        echo "<td> Name </td>";
        echo "<td> Units </td>";
        echo "<td> Quantity </td>";
        echo "<td> Price </td>";
        echo "<td> Country </td>";
        echo "</tr>";
        while($row = mysqli_fetch_assoc($result)){
            echo "<tr>";
            echo "<td>".$row["name"]."</td>";
            echo "<td>".$row["units"]."</td>";
            echo "<td>".$row["quantity"]."</td>";
            echo "<td>$".$row["price"]."</td>";
            echo "<td>".$row["country"]."</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
?>
<?php include_once("_footer.html"); ?>